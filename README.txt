Modello 3D del laboratorio esportato da Hololens 2.
Questo progetto è stato usato per eseguire test sulla mesh; lo scopo era quello di posizionare ologrammi in modo persistente su una mesh da Unity.
Il problema è che al primo avvio dell'applicazione il modello esportato prima di essere ancorato alla mesh dipende dalle coordinate dell'utente, in altre parole non c'è modo di far coincidere il modello con la mesh.
